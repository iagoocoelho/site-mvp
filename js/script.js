
// ADD GREY RECTANGLE CLASS
$(document).ready(function() {
    $('<span>', {
        class: 'grey-rectangle',
    }).appendTo('section#services .container');
    $(".grey-rectangle").addClass("fixed");
});


// RESPONSIVE PHONE MENU
$('.menu-toggle').click(function() {

    $('ul').toggleClass('opening');
    $(this).toggleClass('open');
})

$('.close-menu').click(function() {

    $('ul').toggleClass('opening');
    $(this).toggleClass('open');
});


// EXPAND & COMPRESS BUTTON (PORTIFOLIO)


// CPUE EXPAND & COMPRESS

$( "button#btn-cpue" ).click(function() {
    $("#cpue").toggle("slow");

    if ($('#btn-cpue').attr('class') == 'fa fa-expand') {
        $('#btn-cpue').removeClass('fa fa-expand').addClass('fa fa-compress')
    } else {
        $('#btn-cpue').removeClass('fa fa-compress').addClass('fa fa-expand');
    }
});


// TESTE EXPAND & COMPRESS

$( "button#btn-teste" ).click(function() {
    $("#teste").toggle("slow");

    if ($('#btn-teste').attr('class') == 'fa fa-expand') {
        $('#btn-teste').removeClass('fa fa-expand').addClass('fa fa-compress')
    } else {
        $('#btn-teste').removeClass('fa fa-compress').addClass('fa fa-expand');
    }
});